package com.manager.pmaintain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PmaintainApplication {

	public static void main(String[] args) {
		SpringApplication.run(PmaintainApplication.class, args);
	}

}
