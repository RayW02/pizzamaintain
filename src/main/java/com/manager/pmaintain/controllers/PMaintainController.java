package com.manager.pmaintain.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manager.pmaintain.models.Pizza;

//Spring Anotation
@RestController
@RequestMapping("/pMaintain")
public class PMaintainController {
    private List<Pizza> pizzas = new ArrayList<Pizza>();

    @GetMapping("/list")
    public List<Pizza> getList(){
        return pizzas;
    }

    @PostMapping("/add")
    public void addPizza(@RequestBody Pizza pizza){
        pizzas.add(pizza);
        
    }
}
