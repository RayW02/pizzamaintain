package com.manager.pmaintain.models;

public class Pizza {
    private String name;
    private int price;
    private String description;
    private String category;
    private String status;
    private String size;
    private String base;
    
    
    public Pizza() {
    }
    
    public Pizza(String name, int price, String description, String category, String status, String size, String base) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.category = category;
        this.status = status;
        this.size = size;
        this.base = base;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getSize() {
        return size;
    }
    public void setSize(String size) {
        this.size = size;
    }
    public String getBase() {
        return base;
    }
    public void setBase(String base) {
        this.base = base;
    }
    
}
